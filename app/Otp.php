<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Otp extends Model
{
    protected $table = "otp";
    public $incrementing = false;
    public $primaryKey = 'id';
    protected $guarded=[];

    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function otp() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
