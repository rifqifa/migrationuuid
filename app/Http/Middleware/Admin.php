<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::User()->isAdmin()) {
            return $next($request);
        }
        abort(403);
    }
}
