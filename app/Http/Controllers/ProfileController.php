<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function show() {
        $data['user'] = auth()->user();
        return response()->json([
            'response_code' => '00',
            'response_message' => 'data berhasil ditampilkan',
            'data'=>$data
        ]);
    }

    public function update(Request $request) {
        $user = auth()->user();

        $user->update([
            'name' => $request->name,
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data berhasil diubah',
            'data' => $user
        ]);

    }
}
