<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class PasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|confirmed|min:6',

        ]);
        $user_id = User::where('email', '=', $request->email)->first();
        $User = $user_id->update([
            'password' => bcrypt(request('password'))
        ]);
        
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Password berhasil diubah',
            'data' => $user_id
        ], 200);
    }
}
