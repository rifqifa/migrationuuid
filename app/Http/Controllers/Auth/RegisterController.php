<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Otp;
use Mail;
use App\Events\UserRegisterEvent;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $dateTime = Carbon::now();
        $dateTime->modify('+5 minutes');
        

        request()->validate([
            'name' => ['required'],
            'email' => ['email', 'unique:users,email']
        ]);

        do {
            $otp_code = mt_rand(100000, 999999);
            $check = Otp::where ('otp', $otp_code)->first();
        } while ($check);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
        ]);

        $otp = Otp::create([
            'user_id' => $user->id,
            'otp' => $otp_code,
            'valid_until' => $dateTime
        ]);

        event(new UserRegisterEvent($user, $otp));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'silahkan cek email',
            'data' => $user
        ],200);
    }
}
