<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Otp;
use Carbon\Carbon;
use App\User;
use Mail;
use App\Events\UserRegisterEvent;

class RegenerateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $dateTime = Carbon::now();

        $user_id = User::where('email', '=', $request->email)->first();
        if ($user_id == null) {
            return response()->json([
                'response_code' => '00',
                'response_message' => 'user tidak ditemukan'
            ],200);
        } else {
            do {
                $otp_code = mt_rand(100000, 999999);
                $check = Otp::where ('otp', $otp_code)->first();
            } while ($check);

        $otp = Otp::where('user_id', '=', $user_id->id)->first();
        $regenerate = $otp->update([
            'otp' => $otp_code,
            'valid_until' => $dateTime->modify('+5 minutes')
            ]);
            
        event(new UserRegisterEvent($user_id, $otp));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'silahkan cek email',
        ],200);}
    }
}
