<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Otp;
use Carbon\Carbon;
use App\User;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate ([
            'otp'=>'required'
        ]);

        $otp = $request->otp;
        $current_date = Carbon::now();

        $validate = Otp::where('otp', '=', $otp)->first();

        if ($validate === null) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'otp code tidak ditemukan',
            ], 200);
            
         } else {
             if ($validate->valid_until < $current_date) {
               return response()->json([
                'response_code' => '01',
                'response_message' => 'otp code sudah tidak berlaku, silahkan generate ulang'
               ], 200);
             } else {
                $verified = User::where('id', '=', $validate->user_id)->update([
                    'email_verified_at' => $current_date
                    ]);
                $validate->delete();
                return response()->json([
                    'response_code' => '00',
                    'response_message' => 'verified',
                 ], 200);
            }
         }
    }
}
