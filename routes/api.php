<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->group(function() {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('verification', 'VerificationController');
    Route::post('regenerate', 'RegenerateController');
    Route::post('password', 'PasswordController');

});

Route::group([
    'middleware' => ['api', 'VerifEmail', 'auth:api'],
], function() {
    Route::get('profile/show', 'ProfileController@show');
    Route::post('profile/update', 'ProfileController@update');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaign',
], function (){
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store');
    Route::get('/', 'CampaignController@index');
    Route::get('/{id}', 'CampaignController@detail');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blog',
], function (){
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});