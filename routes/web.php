<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::post('/', 'RoleController@store');

// Route::middleware(['Admin', 'VerifEmail'])->group(function() {
//     Route::get('/route-2', 'LoginController@Admin');
// });

// Route::get('/route-1', 'LoginController@Verif')->middleware('VerifEmail');

Route::view('/{any?}', 'app')->where('any', '.*');