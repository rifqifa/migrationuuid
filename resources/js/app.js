import Vue from  'vue'
import router from './router.js'
import App from './App.vue'
import vuetify from './plugins/vuetify.js'
import './bootstrap.js';
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        count:1
    },
    mutation: {
        donate(state) {
            state.count++
        }
    },
    actions: {

    }
})

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    components: {
        App
    },
});
