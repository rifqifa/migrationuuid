<?php

use Illuminate\Database\Seeder;
use App\Campaign;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campaign::create([
            'title' => Str::random(10),
            'description' => Str::random(100)
        ]);
    }
}
